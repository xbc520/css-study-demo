# CSS 学习

>  一个 `css` 初学者建立的仓库，用来记录 `css` 的学习。
>
> 从实例中感悟 `css` 的奥妙

## 1、交错动画

> :tea:  *通过设置不同的延迟时间，达到动画交错播放的效果。*
>
> :coffee: 可以通过css动画延迟（`delay`）属性来实现。

``` html
    <div class="loading">
        <div class="dot bg1"></div>
        <div class="dot bg2"></div>
        <div class="dot bg3"></div>
        <div class="dot bg4"></div>
        <div class="dot bg5"></div>
    </div>

    <style>
        body {
            display: flex;
            height: 100vh;
            justify-content: center;
            align-items: center;
        }

        .loading {
            display: flex;
        }

        .dot {
            position: relative;
            width: 2em;
            height: 2em;
            margin: 0.8em;
            border-radius: 50%;
        }

        .dot::before {
            position: absolute;
            content: "";
            width: 100%;
            height: 100%;
            background: inherit;
            border-radius: inherit;
            animation: wave 2s ease-out infinite;
        }

        .bg1 {
            background: #7ef9ff; 
        }
        .bg1::before {
            animation-delay: 0.2s;
        }

        .bg2 {
            background: #89cff0;
        }
        .bg2::before {
            animation-delay: 0.4s;
        }

        .bg3 {
            background: #4682b4;
        }
        .bg3::before {
            animation-delay: 0.6s;
        }

        .bg4 {
            background: #0f52ba;
        }
        .bg4::before {
            animation-delay: 0.8s;
        }

        .bg5 {
            background: #000080;
        }
        .bg5::before {
            animation-delay: 1s;
        }


        @keyframes wave {
            50%,
            75% {
                transform: scale(2.5);
            }

            80%,
            100% {
                opacity: 0;
            }
        }
    </style>
```

> :loudspeaker: **::before**，创建一个伪元素，其将成为匹配选中元素中的第一个子元素。通常通过 `content` 属性来为元素添加修饰性的内容。此元素默认为行内元素。
>
> :zap:  很多动画效果都需要使用 `::before` 与 `::after`

``` css
/* Add a heart before links */
a::before {
  content: "♥";
}
```

## 2、字体单位大小

`CSS` 中存在很多可以描述单位的尺寸，比如 `px`、 `em`、 `rem`、 `vw`、 `vh`等。

### 2.1、CSS长度单位

- 绝对长度单位，表示一个固定值，不会改变，***不利于页面渲染***。

  - in：英寸
  - cm：厘米
  - mm：毫米

- 相对长度单位，随着参考值的变化而变化。

  - px：像素

  - em：元素字体高度

    > body里声明font-size:62.5%。即全局声明`1em = 16px * 62.5% = 10px`
    >
    > 之后可以把`em`当做`px`使用。当然此时，`1em = 10px`
    >
    > 如果在父容器里说明了`font-size:20px`,那么在子容器里的`1em`就等于`20px`。

  - %：百分比

  - rem：根元素的font-size

    > ``` css
    > html {
    >   font-size: 10px; /* 不建议设置 font-size: 62.5%; 在 IE 9-11 上有偏差，具体表现为 1rem = 9.93px。 */
    > }
    > 
    > .sqaure {
    >   width: 5rem;  /* 50px */
    >   height: 5rem; /* 50px */
    > }
    > 
    > <style>
    >   html{ font-size: 20px; }
    >   body{ 
    >     font-size: 1.4rem;  /* 1rem = 28px */
    >     padding: 0.7rem;  /* 0.7rem = 14px */
    >   } 
    >   div{
    >     padding: 1em;  /* 1em = 28px */
    >   }
    >   span{
    >     font-size:1rem;  /* 1rem = 20px */
    >     padding: 0.9em;  /* 1em = 18px */
    >   }
    > </style>
    > 
    > <html>
    >   <body>
    >     <div>   
    >       <span></span>  
    >     </div>
    >   </body>
    > </html>
    > ```

  - vw：视窗宽度，1vw = 视窗宽度的1%

  - vh：视窗高度，1vh = 视窗高度的1%

